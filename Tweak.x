@interface EXTERNAL
-(id)photoSelectionManager;
-(NSSet*)selectedAssets;
-(NSURL*)mainFileURL;
@end

%hook PUPhotosGridViewController
-(NSString*)localizedSelectionTitle {
  double bytes=0;
  unsigned int count=0;
  for (id asset in [[self photoSelectionManager] selectedAssets]){
    NSNumber* fileSize;
    if([[asset mainFileURL] getResourceValue:&fileSize forKey:NSURLFileSizeKey error:NULL]){
      bytes+=fileSize.unsignedLongLongValue;
      count++;
    }
  }
  if(!count){return %orig;}
  int i;
  for (i=0;i<3 && bytes>=1024;i++){bytes/=1024;}
  NSString* const key[]={@"%.0f bytes",@"%.1f KB (1.0)",@"%.1f MB (1.0)",@"%.1f GB (1.0)"};
  NSBundle* bPL=[NSBundle bundleWithIdentifier:@"com.apple.PhotoLibrary"];
  NSBundle* bURL=[NSBundle bundleWithIdentifier:@"com.apple.Foundation"];
  return [NSString stringWithFormat:[bURL localizedStringForKey:@"%@ (%@)" value:nil table:@"URL"],
   (count==1)?[bPL localizedStringForKey:@"1_ALBUM_OTHER" value:nil table:@"PhotoLibrary"]:
   [NSString localizedStringWithFormat:[bPL localizedStringForKey:@"ALBUM_OTHER_COUNT_FORMAT"
   value:nil table:@"PhotoLibrary"],[NSNumber numberWithUnsignedLongLong:count]],
   [NSString localizedStringWithFormat:[bURL localizedStringForKey:key[i]
   value:nil table:@"URL"],bytes]];
}
%end
